import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private http: HttpClient
  ) { }

  /** HTTP通信 実行 */
  async send<Req, Res>(method: string, path: string, request?: Req): Promise<HttpResponse<Res>> {
    try {

      const url = path;
      console.log(path)
      console.log('[HttpService - url] ', url);

      let httpParams: HttpParams = new HttpParams();
      if ((method === 'GET' || method === 'DELETE') && request) {
        for (const requestKey of Object.keys(request)) {
          if (request[requestKey]) {
            httpParams = httpParams.append(requestKey, request[requestKey]);
          }
        }
      }

      const response = await this.http.request<Res>(method, url, {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
        responseType: 'json',
        params: httpParams,
        body: request
      }).toPromise();
      console.log('[HttpService - response] ', response);
      return { ok: true, status: 200, body: response } as HttpResponse<Res>;
    } catch (error) {
      console.log('[HttpService - error] ', error);

      if (error instanceof HttpErrorResponse) {
        return { ok: false, status: error.status, body: error.error } as HttpResponse<any>;
      } else {
        return { ok: false, body: undefined } as HttpResponse<Res>;
      }
    } finally {

    }
  }
}
