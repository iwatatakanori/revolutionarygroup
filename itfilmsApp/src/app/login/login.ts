export interface LoginUser {
    user: string;
    login: boolean;
}
  
export interface AuthParam {
    id: string;
    password: string;
  }