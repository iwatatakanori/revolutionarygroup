export interface ILoginRequest {
  login_id: string;
  password: string;
}