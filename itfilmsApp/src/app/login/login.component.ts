import { Component, OnInit } from '@angular/core';
import { AuthParam, LoginUser } from './login';
import { HttpService } from '../common/http.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ILoginRequest } from './entities/request/i.login.request';
import { ILoginResponse } from './entities/response/i.login.response';
import { URLConst } from '../common/urlconstant';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  // ユーザーID
  id : string;
  // パスワード
  password :string;

  blnPassVisible: boolean = true;
  blnLoading: boolean = false;
  strNextPage: string;
  strLoadingMsg: string = "ログイン処理中です";
  errors: string[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: HttpService
  ) { }


  ngOnInit(): void {


    //ActivatedRouteを使ってログイン成功時の移動先を取得します。
    if (this.route.snapshot.queryParams.hasOwnProperty('url')) {
      this.strNextPage = this.route.snapshot.queryParams.url;
    } else {
      this.strNextPage = "";
    }

  }

  public async login() {
    //エラー配列クリア
    this.errors = [];

    if (this.id === '') this.errors.push("ユーザー名を空白にはできません");
    if (this.password === '') this.errors.push("パスワードを空白にはできません");

    //この段階でエラーなら戻る
    if (0 < this.errors.length) {
      return;
    }

    const response = await this.service.send<ILoginRequest, ILoginResponse>('POST', URLConst.LOGIN_API_PATH, this.setRequestParam());
    console.log(response);

    if (response.body.status == 'success') {

      // ダッシュボード画面へ遷移
      this.router.navigate(["/dashboard"]);

    } else {
      //認証失敗時はローカルストレージのデータもクリアします。
      localStorage.removeItem('token');
      this.errors.push("ユーザーIDまたはパスワードが不正です");
    }
  }
  // リクエストパラメータを設定
  private setRequestParam(): ILoginRequest {

    // リクエストパラメータを設定
    var request: ILoginRequest = {
      login_id: this.id,
      password: this.password
    };

    return request;
  }
}
